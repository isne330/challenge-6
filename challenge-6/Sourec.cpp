#include <iostream>
#include "fraction.h"
using namespace std;
int main(){

	int num1, denom1, num2, denom2;
	cout << "\n\nInput First num     is number : ";
	cin >> num1;
	cout << "Input First denom   is number : ";
	cin >> denom1;
	cout << "Input Second num    is number : ";
	cin >> num2;
	cout << "Input Secound denom is number : ";
	cin >> denom2;
	fraction n1(num1,denom1);
	fraction n2(num2,denom2);
	fraction n3;
	n3 = n1 + n2;
	cout << "\nFirst  number + Second number  = "<<n3.getnum() << "/" << n3.getdenom()<<endl;
	n3 = n1 - n2;
	cout << "First  number - Second number  = " << n3.getnum() << "/" << n3.getdenom()<<endl;
	n3 = n1 * n2;
	cout << "First  number * Second number  = " << n3.getnum() << "/" << n3.getdenom() << endl;
	n3 = n1 / n2;
	cout << "First  number / Second number  = " << n3.getnum() << "/" << n3.getdenom() << endl;
	n3 = n1++;
	cout << "First  number ++ = "<< n3.getnum() << "/" << n3.getdenom() << endl;
	n3 = n1--;
	cout << "First  number -- = " << n3.getnum() << "/" << n3.getdenom() << endl;
	n3 = n2++;
	cout << "Second number ++ = "<<n3.getnum() << "/" << n3.getdenom() << endl;
	n3 = n2--;
	cout << "Second number -- = " << n3.getnum() << "/" << n3.getdenom() << endl;
	if (n1 == n2)	cout << "First number equal Second number" << endl;
	if (n1 != n2)	cout << "First number not equal Second number" << endl;
	if (n1 < n2)	cout << "First number smaller than Second number" << endl;
	if (n1 > n2)	cout << "First number greater than Second number" << endl;
	if (n1 <= n2)	cout << "First number smaller than or equal Second number" << endl;
	if (n1 >= n2)	cout << "First number greater than or equal Second number" << endl;
	return 0;
}                                                                                                        
#ifndef FRACTION_H
#define FRACTION_H
#include <iostream>
using namespace std;
class fraction{

	private:	
	int num;
	int denom;
	int irreducible(int,int);

	public:
	fraction();//constructor
	fraction(int newnum);
	fraction(int newnum, int newdenom);
	int getnum();//accsess
	int getdenom();
	void setnum(int new_num);//mulato
	void setdenom(int new_denom);
	friend ostream &operator <<(ostream &output, fraction &fr);//cin 
	friend istream &operator >>(istream &input, fraction &fr);//cout
	friend bool operator ==(fraction n1, fraction n2);//operator
	friend bool operator !=(fraction n1, fraction n2);
	friend bool operator <(fraction n1, fraction n2);
	friend bool operator <=(fraction n1, fraction n2);
	friend bool operator >(fraction n1, fraction n2);
	friend bool operator >=(fraction n1, fraction n2);
	friend fraction operator +(fraction n1, fraction n2);
	friend fraction operator -(fraction n1, fraction n2);
	friend fraction operator *(fraction n1, fraction n2);
	friend fraction operator /(fraction n1, fraction n2);
	friend fraction operator ++(fraction &n);//urinary
	friend fraction operator --(fraction &n);
		
};
#endif //FRACTION_H
#include <iostream>
#include <cstdlib>
#include "fraction.h"
using namespace std;
fraction::fraction(){
	num = 1;
	denom = 1;
}
fraction::fraction(int newnum){
	num = newnum;
	denom = 1;
}
fraction::fraction(int newnum, int newdenom){
	num = newnum;
	denom = newdenom;
}
int fraction::getnum(){
		int tmp=irreducible(num,denom);

	return num/tmp;
}
int fraction::getdenom(){
		int tmp=irreducible(num,denom);

	return denom/tmp;
}
void fraction::setnum(int newnum){
	num = newnum;
}
void fraction::setdenom(int newdenom){
	denom = newdenom;
}
fraction operator +(fraction n1, fraction n2){
	fraction result;
	result.num = (n1.num * n2.denom) + (n1.denom * n2.num);
	result.denom = n1.denom * n2.denom;
	return result;
}
fraction operator -(fraction n1, fraction n2){
	fraction result;
	result.num = (n1.num * n2.denom) - (n1.denom * n2.num);
	result.denom = n1.denom * n2.denom;
	return result;
}
fraction operator *(fraction f1, fraction f2){
	fraction result;
	result.num = f1.num * f2.num;
	result.denom = f1.denom * f2.denom;
	return result;
}
fraction operator /(fraction n1, fraction n2){
	fraction result;
	result.num = n1.num * n2.denom;
	result.denom = n1.denom * n2.num;
	return result;
}
fraction operator ++(fraction &n){
	n = n + 1;
	return n;
}
fraction operator --(fraction &n){
	n = n - 1;
	return n;
}
ostream &operator <<(ostream &output, fraction &n){
	int tmp=n.irreducible(n.num,n.denom);
	output << n.num/tmp << "/" << n.denom/tmp << endl;
	return output;
}
istream &operator >>(istream &input, fraction &n){
	input >> n.num >> n.denom;
	return input;
}
bool operator ==(fraction n1, fraction n2){
	return((n1.num == n2.num)&&(n1.denom == n2.denom));
}
bool operator !=(fraction n1, fraction n2){
	return((n1.num != n2.num)&&(n1.denom != n2.denom));
}
bool operator <(fraction n1, fraction n2){
	return((n1.num < n2.num)&&(n1.denom < n2.denom));
}
bool operator <=(fraction n1, fraction n2){
	return((n1.num <= n2.num)&&(n1.denom <= n2.denom));
}
bool operator >(fraction n1, fraction n2){
	return((n1.num > n2.num)&&(n1.denom > n2.denom));
}
bool operator >=(fraction n1, fraction n2){
	return((n1.num >= n2.num)&&(n1.denom >= n2.denom));
}
int fraction::irreducible(int num,int denom){
int tmp=1;
	for (int i = 2; i <= denom; i++){
		if (num%i == 0 && denom%i == 0){
			tmp = i;
		}
	}

	return tmp;
}

	

